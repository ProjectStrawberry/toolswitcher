package com.gmail.jd4656.switcher;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class EventListeners implements Listener {
    private Switcher plugin;

    EventListeners(Switcher p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        if (event == null || event.getHand() == null || !event.getHand().equals(EquipmentSlot.HAND) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
        Player player = event.getPlayer();
        if (!plugin.toolStatus.contains(player.getUniqueId())) return;

        Block block = event.getClickedBlock();
        ItemStack item = event.getItem();

        if (block == null) return;

        if (plugin.infoCmd.contains(player.getUniqueId())) {
            player.sendMessage(ChatColor.DARK_GREEN + "Block material: " + ChatColor.GOLD + block.getType());
            plugin.infoCmd.remove(player.getUniqueId());
            event.setCancelled(true);
            return;
        }

        String itemString = plugin.tools.get(block.getType());
        if (itemString == null) return;

        PlayerInventory inv = player.getInventory();

        if (itemString.equals("pickaxe")) {
            if (item != null) {
                if (item.getType().equals(Material.NETHERITE_PICKAXE) || item.getType().equals(Material.DIAMOND_PICKAXE) || item.getType().equals(Material.GOLDEN_PICKAXE) || item.getType().equals(Material.IRON_PICKAXE) || item.getType().equals(Material.STONE_PICKAXE)) return;
            }
            if (inv.contains(Material.NETHERITE_PICKAXE)) {
                switchItem(Material.NETHERITE_PICKAXE, player);
                return;
            }
            if (inv.contains(Material.DIAMOND_PICKAXE)) {
                switchItem(Material.DIAMOND_PICKAXE, player);
                return;
            }
            if (inv.contains(Material.GOLDEN_PICKAXE)) {
                switchItem(Material.GOLDEN_PICKAXE, player);
                return;
            }
            if (inv.contains(Material.IRON_PICKAXE)) {
                switchItem(Material.IRON_PICKAXE, player);
                return;
            }
            if (inv.contains(Material.STONE_PICKAXE)) {
                switchItem(Material.STONE_PICKAXE, player);
                return;
            }
        }

        if (itemString.equals("shovel")) {
            if (item != null) {
                if (item.getType().equals(Material.NETHERITE_SHOVEL) || item.getType().equals(Material.DIAMOND_SHOVEL) || item.getType().equals(Material.GOLDEN_SHOVEL) || item.getType().equals(Material.IRON_SHOVEL) || item.getType().equals(Material.STONE_SHOVEL)) return;
            }
            if (inv.contains(Material.NETHERITE_SHOVEL)) {
                switchItem(Material.NETHERITE_SHOVEL, player);
                return;
            }
            if (inv.contains(Material.DIAMOND_SHOVEL)) {
                switchItem(Material.DIAMOND_SHOVEL, player);
                return;
            }
            if (inv.contains(Material.GOLDEN_SHOVEL)) {
                switchItem(Material.GOLDEN_SHOVEL, player);
                return;
            }
            if (inv.contains(Material.IRON_SHOVEL)) {
                switchItem(Material.IRON_SHOVEL, player);
                return;
            }
            if (inv.contains(Material.STONE_SHOVEL)) {
                switchItem(Material.STONE_SHOVEL, player);
                return;
            }
        }

        if (itemString.equals("axe")) {
            if (item != null) {
                if (item.getType().equals(Material.NETHERITE_AXE) || item.getType().equals(Material.DIAMOND_AXE) || item.getType().equals(Material.GOLDEN_AXE) || item.getType().equals(Material.IRON_AXE) || item.getType().equals(Material.STONE_AXE)) return;
            }
            if (inv.contains(Material.NETHERITE_AXE)) {
                switchItem(Material.NETHERITE_AXE, player);
                return;
            }
            if (inv.contains(Material.DIAMOND_AXE)) {
                switchItem(Material.DIAMOND_AXE, player);
                return;
            }
            if (inv.contains(Material.GOLDEN_AXE)) {
                switchItem(Material.GOLDEN_AXE, player);
                return;
            }
            if (inv.contains(Material.IRON_AXE)) {
                switchItem(Material.IRON_AXE, player);
                return;
            }
            if (inv.contains(Material.STONE_AXE)) {
                switchItem(Material.STONE_AXE, player);
                return;
            }
        }

        if (itemString.equals("shears")) {
            if (item != null && item.getType().equals(Material.SHEARS)) return;
            if (inv.contains(Material.SHEARS)) {
                switchItem(Material.SHEARS, player);
            }
        }
    }

    private void switchItem(Material material, Player player) {
        PlayerInventory inv = player.getInventory();
        if (!inv.contains(material)) return;

        int index = inv.first(material);
        if (index < 9) {
            inv.setHeldItemSlot(index);
        } else {
            ItemStack newItem = inv.getItem(index);
            ItemStack oldItem = inv.getItemInMainHand();

            inv.setItemInMainHand(newItem);
            inv.setItem(index, oldItem);
        }
        player.updateInventory();
    }
}
