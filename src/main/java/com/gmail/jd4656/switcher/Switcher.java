package com.gmail.jd4656.switcher;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class Switcher extends JavaPlugin {
    Switcher plugin;
    Map<Material, String> tools = null;
    List<UUID> toolStatus = new ArrayList<>();
    List<UUID> infoCmd = new ArrayList<>();
    Map<Material, String> toolToString = new HashMap<>();
    FileConfiguration config = null;

    @Override
    public void onEnable() {
        plugin = this;
        this.saveDefaultConfig();
        config = this.plugin.getConfig();
        config.options().copyDefaults(false);
        plugin.saveConfig();

        loadTools();

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("switcher").setExecutor(new CommandSwitcher(this));

        this.getLogger().info("ToolSwitcher loaded.");
    }

     void loadTools() {
        tools = new HashMap<>();
        Set<String> keys = config.getConfigurationSection("materials").getKeys(false);

        for (String key : keys) {
            Material curMaterial = Material.matchMaterial(key);
            if (curMaterial == null) {
                plugin.getLogger().warning("Invalid material in config.yml: " + curMaterial);
                continue;
            }
            tools.put(curMaterial, config.getString("materials." + key));
        }
    }
}
