package com.gmail.jd4656.switcher;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSwitcher implements CommandExecutor {

    private Switcher plugin;

    CommandSwitcher(Switcher p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length > 0) {
            if (args[0].equals("reload")) {
                if (!sender.hasPermission("switcher.admin")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }

                plugin.reloadConfig();
                plugin.config = plugin.getConfig();
                plugin.loadTools();
                sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
                return true;
            }

            if (args[0].equals("info")) {
                if (!sender.hasPermission("switcher.admin")) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                    return true;
                }

                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                    return true;
                }

                Player player = (Player) sender;

                plugin.infoCmd.add(player.getUniqueId());
                sender.sendMessage(ChatColor.DARK_GREEN + "Click a block to view the material type.");
                return true;
            }
        }

        if (!sender.hasPermission("switcher.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }


        Player player = (Player) sender;

        if (!plugin.toolStatus.contains(player.getUniqueId())) {
            plugin.toolStatus.add(player.getUniqueId());
            player.sendMessage(ChatColor.DARK_GREEN + "You have enabled auto tool switching.");
            return true;
        } else {
            plugin.toolStatus.remove(player.getUniqueId());
            player.sendMessage(ChatColor.DARK_GREEN + "You have disabled auto tool switching.");
            return true;
        }
    }
}
